import {CreateTable} from './table.js';

export default class UserTable extends CreateTable(
    'users',
    {
        username: '', 
        email: '', 
        password: '', 
        token: ''
    }
) {
    constructor() {
        super();
        this.tokenDuration = 1440; //minutes
    }

    signIn(username, password) {
        const index = this.findByKey('username', username);
        if (index < 0) {
            return;
        }
        
        const user = this.get(index);
        const dbHash = this.cryptPassword(user.password);
        const hash = this.cryptPassword(password);

        if (dbHash == hash) {
            this.createToken(index);
            return user.token;
        }
        
        return;
    }

    signOut(userID) {
        let user = this.get(userID);
        user.token = '';
    }

    signUp(username, email, password) {
        if (this.checkUser(username)) {
            console.warn(`User '${username}' is already exists!`);
            return -1;
        }

        const hash = this.cryptPassword(password);

        return this.add({
            username: username, 
            email: email, 
            password: hash, 
            token: ''
        });
    }

    checkUser(username) {
        const index = this.findByKey('username', username);
        return (index >= 0);
    }

    validate(token) {
        const index = this.findByKey('token', token);
        if (index >= 0 && token) {
            const info = this.readToken(token);
            if (info.endDate > Date.now()) {
                return index;
            }
        }

        return -1;
    }

    cryptPassword(password) {
        return password;
    }

    createToken(index) {
        const user = this.get(index);

        const currentDate = new Date();
        const endDate = new Date(currentDate);
        const minutes = endDate.getMinutes();
        endDate.setMinutes(minutes + this.tokenDuration);

        const info = {
            username: user.username,
            currentDate: currentDate.getTime(),
            endDate: endDate.getTime()
        };

        user.token = JSON.stringify(info);
        this.export();
    }

    readToken(token) {
        return JSON.parse(token);
    }
}
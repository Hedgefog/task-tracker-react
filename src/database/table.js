export function CreateTable(name, scheme) {
    if (!name || typeof(name) != 'string') {
        console.error('Invalid table name');
        return undefined;
    }
    
    if (!scheme || toString.call(scheme) !== '[object Object]') {
        console.error('Invalid scheme type!');
        return undefined;
    }

    let table = class Table {
        constructor() {
            this.cells = {};
            this.import();
        }

        add(scheme, doExport = true) {
            let index = this.lastIndex() + 1;
            index = this.set(index, scheme, doExport);

            if (index < 0) {
                this.remove(index, 1, false);
            }

            return index;
        }

        remove(index, count = 1, doExport = true) {
            delete this.cells[index];

            if (doExport) {
                this.export();
            }
        }

        set(index, scheme, doExport = true) {
            if (index < 0) {
                console.log('Index of cell must be higher then 0');
                return;
            }

            const isValid = this.checkScheme(scheme);

            if (!isValid) {
                console.error('Invalid scheme!');
                return -1;
            }
            
            this.cells[index] = scheme;

            if (doExport) {
                this.export();
            }

            return index;
        }

        get(index) {
            return this.cells[index];
        }

        size() {
            return this.cells.length;
        }

        lastIndex() {
            const keys = Object.keys(this.cells);

            let index = 0;
            if (keys.length > 0) {
                index = parseInt(keys[keys.length-1]);
            }

            return index;
        }

        import() {
            if (!window.localStorage.hasOwnProperty(this.constructor.Name)) {
                console.warn('Table is not a found in local storage!');
                return;
            }

            const str = window.localStorage.getItem(this.constructor.Name);
            const cells = JSON.parse(str);
            
            if (cells) {
                this.cells = cells;
            } else {
                console.error('Table is damaged');
            }
        }

        export() {
            const str = JSON.stringify(this.cells);
            window.localStorage.setItem(this.constructor.Name, str);
        }

        findByKey(key, value) {
            for (let i in this.cells) {
                const cell = this.cells[i];
                if (cell[key] == value) {
                    return parseInt(i);
                }
            }

            return -1;
        }

        findAllByKeys(arr) {
            this.cells.find((cell)=> {
                for (let i = 0; i < arr.length; ++i) {
                    const key = arr[i].key;
                    const value = arr[i].value;

                    if (cell[key] != value) {
                        return false;
                    }
                }

                return true;
            });
        }

        checkScheme(scheme) {
            const _scheme = this.constructor.Scheme;

            const keys = Object.keys(scheme);
            const _keys = Object.keys(_scheme);
            
            if (keys.length != _keys.length) {
                return false;
            }

            for (let i = 0; i < _keys.length; ++i) {
                const key = _keys[i];
                if (!scheme.hasOwnProperty(key)) {
                    return false;
                } else {
                    if (typeof(scheme[key]) != typeof(_scheme[key])) {
                        return false;
                    }
                }
            }

            return true;
        }
    };

    table.Name = name;
    table.Scheme = scheme;

    return table;
}
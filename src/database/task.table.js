import {CreateTable} from './table.js';

export default class TaskTable extends CreateTable(
    'tasks',
    {
        title: '',
        description: '',
        category: 0, 
        priority: 0,
        owner: 0,
        users: [
            {id: 0, role: 0}
        ],
        history: [
            {category: 0, time: 0}
        ]
    }
) {
    static get UserRole() {
        return {
            Viewer: 0,
            Developer: 1,
            Admin: 2
        };
    }

    static get Permissions() {
        return {
            View: 0,
            Move: 1,
            Edit: 2,
            AddUsers: 3,
            Delete: 4,
        };
    }

    static get Category() {
        return {
            Pending: 0,
            ToDo: 1,
            InProgress: 2,
            Done: 3
        };
    }

    static getRolePermissions(role) {
        if (role == TaskTable.UserRole.Viewer) {
            return [0];
        } else if (role == TaskTable.UserRole.Developer) {
            return [0, 1, 2];
        } else if (role == TaskTable.UserRole.Admin) {
            return [0, 1, 2, 3];
        }

        return [];
    }

    constructor() {
        super();
    }

    createTask(owner, title, description, priority) {
        const scheme = {
            title: title,
            description: description,
            priority: parseInt(priority),
            category: 0,
            owner: owner,
            users: [
                {
                    id: owner, 
                    role: TaskTable.UserRole.Admin
                }
            ],
            history: [
                {category: 0, time: Date.now()}
            ],
        };

        return this.add(scheme);
    }

    editTask(userID, taskID, title, description, priority) {
        if (this.checkPermission(userID, taskID, TaskTable.Permissions.Delete)) {
            let task = this.get(taskID);
            task.title = title;
            task.description = description;
            task.priority = priority;

            this.export();

            return true;
        }

        return false;
    }

    canMoveTask(taskID, category) {
        const task = this.get(taskID);

        if (task.category) {
            if ((task.category == TaskTable.Category.Pending) && (category == TaskTable.Category.Done)) {
                return false;
            } else if (task.category == TaskTable.Category.Done) {
                return false;
            } else if (category == TaskTable.Category.Pending) {
                return false;
            }
        }

        return true;
    }

    moveTask(userID, taskID, category) {
        if (this.checkPermission(userID, taskID, TaskTable.Permissions.Move)) {
            if (!this.canMoveTask(taskID, category)) {
                return false;
            }
            
            let task = this.get(taskID);
            task.category = category;

            task.history.push({
                category: category,
                time: Date.now()
            });

            this.export();
            return true;
        }

        return false;
    }

    deleteTask(userID, taskID) {
        if (this.checkPermission(userID, taskID, TaskTable.Permissions.Delete)) {
            this.remove(taskID);
            return true;
        }

        return false;
    }

    getTask(userID, taskID) {
        if (this.checkPermission(userID, taskID, TaskTable.Permissions.View)) {
            let task = this.get(taskID);
            task.id = taskID;
            
            return task;
        }
    }

    addUserToTask(userID, taskID, otherUserID) {
        let task = this.get(taskID);

        if (userID != task.owner) {
            return this.checkPermission(userID, taskID, TaskTable.Permissions.AddUsers);
        }

        task.users.push(otherUserID);
        this.set(taskID, task);
        
        return true;
    }

    getUserRole(userID, taskID) {
        let task = this.get(taskID);

        if (!task.users) {
            return -1;
        }

        for (let i = 0; i < task.users.length; ++i) {
            let user = task.users[i];
            if (user.id == userID) {
                return user.role;
            }
        }

        return -1;
    }

    getTasks(userID) {
        let tasks = [];
        for (let taskID = 0; taskID <= this.lastIndex(); ++taskID) {
            let task = this.getTask(userID, taskID);

            if (task) {
                tasks.push(task);
            }
        }

        return tasks;
    }

    checkPermission(userID, taskID, permission) {
        const task = this.get(taskID);

        if (!task) {
            return false;
        }

        if (task.owner == userID) {
            return true;
        }

        const role = this.getUserRole(userID, taskID);
        const permissions = TaskTable.getRolePermissions(role);

        return (permissions.indexOf(permission) >= 0);
    }
}
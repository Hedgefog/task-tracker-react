import Service from './service.js';

import UserTable from '../database/user.table.js';
import TaskTable from '../database/task.table.js';

class UserService extends Service {
    constructor() {
        super();

        this.userTable = new UserTable();
        
        this.reset();

        this.registerEvent('signUp');
        this.registerEvent('signIn');
        this.registerEvent('signOut');

        this.importToken();
    }

    reset() {
        this.username = '';
        this.token = '';
        this.signedIn = false;
    }

    signInByToken() {
        const userID = this.getUserID();

        if (this.token && userID >= 0) {
            const data = this.userTable.readToken(this.token);
            this.username = data.username;
            this.signedIn = true;

            this.dispatchEvent('signIn', {token: this.token});
            return true;
        } else {
            this.token = "";
        }

        return false;
    }

    signUp(username, password) {
        const index = this.userTable.signUp(username, '', password);
        const result = (index > 0);

        if (result) {
            this.dispatchEvent('signUp');
        }

        return result;
    }

    signIn(username, password) {
        const token = this.userTable.signIn(username, password);

        if (token) {
            this.token = token;
            this.username = username;
            this.signedIn = true;

            this.dispatchEvent('signIn', {token});
            this.exportToken();
            return true;
        }

        return false;
    }

    signOut() {
        const userID = this.getUserID();

        if (userID >= 0) {
            this.userTable.signOut(userID);
        }
        
        this.dispatchEvent('signOut');
        this.reset();

        this.exportToken();
    }

    getUserID() {
        return this.userTable.validate(this.token);
    }

    importToken() {
        this.token = window.localStorage.getItem('token');
    }

    exportToken() {
        window.localStorage.setItem('token', this.token);
    }
}

export default new UserService();
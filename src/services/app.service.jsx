import Service from './service.js';

import React from 'react';

import EditTaskForm from '../components/edit-task-form.component.jsx';

import TaskService from './task.service.js';

class AppService extends Service {
    constructor() {
        super();

        this.reset();

        this.registerEvent('task-drag');
        this.registerEvent('task-drop');
        this.registerEvent('open-modal');
        this.registerEvent('close-modal');

        this.createTaskModalData = {};
    }

    reset() {
        this.taskDragged = false;
        this.draggedTaskID = -1;
    }

    setTaskDragged(taskID, value) {
        if (this.taskDragged != value) {
            this.taskDragged = value;

            if (this.taskDragged) {
                this.draggedTaskID = taskID;
                this.dispatchEvent('task-drag');
            } else {
                this.draggedTaskID = -1;
                this.dispatchEvent('task-drop');
            }
        }
    }

    openModal(data) {
        this.dispatchEvent('open-modal', data);
    }

    openCreateTaskModal() {
        this.openModal({
            title: 'Create Task',
            body: <EditTaskForm onChange={(data)=> {
                console.log(data);
                this.createTaskModalData = data;
            }}/>,
            okText: 'Create',
            okCallback: ()=> {
                TaskService.createTask(
                    this.createTaskModalData.title, 
                    this.createTaskModalData.description, 
                    this.createTaskModalData.priority
                );

                this.dispatchEvent('close-modal');
            },
            closeCallback: ()=> {
                this.dispatchEvent('close-modal');
            }
        });
    }

    openDeleteAcceptModal(taskID) {
        this.openModal({
            title: 'Delete Task',
            body: <p>Are you sure you want to delete this task?</p>,
            okText: 'Delete',
            okCallback: ()=> {
                TaskService.deleteTask(taskID);
                this.dispatchEvent('close-modal');
            },
            closeCallback: ()=> {
                this.dispatchEvent('close-modal');
            }
        });
    }
}

export default new AppService();
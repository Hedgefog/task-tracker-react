export default class Service {
    constructor() {
        this.events = {};
    }

    registerEvent(key) {
        if (this.checkEvent(key)) {
            console.warn('Event ' + key + ' is already registered.');
            return;
        }

        this.events[key] = {
            disabled: false,
            listeners: []
        };
    }

    unregisterEvent(key) {
        if (!this.checkEvent(key)) {
            console.warn('Event ' + key + ' is not registered.');
            return;   
        }

        delete this.events[key];
    }

    enableEvent(key, value = true) {
        if (!this.checkEvent(key)) {
            return;
        }

        this.events[key].disabled = !value;
    }

    addEventListener(key, callback) {
        if (!this.checkEvent(key)) {
            return;
        }
        
        this.events[key].listeners.push(callback);
    }

    removeEventListener(key, callback) {
        if (!this.checkEvent(key)) {
            return;
        }

        this.events[key].listeners.map((item, i)=> {
            if (item == callback) {
                this.events[key].listeners.splice(i, 1);
            }
        });
    }

    dispatchEvent(key, object = {}) {
        if (!this.checkEvent(key)) {
            return;
        }

        if (!this.events[key].disabled) {
            this.events[key].listeners.map((callback)=> {
                callback(object);
            });
        }
    }

    checkEvent(key) {
        return (!!this.events[key]);
    }
}
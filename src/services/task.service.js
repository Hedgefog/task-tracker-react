import Service from './service.js';

import TaskTable from '../database/task.table.js';

import UserService from '../services/user.service.js';

class TaskService extends Service {
    constructor() {
        super();

        this.taskTable = new TaskTable();

        this.reset();

        this.registerEvent('create');
        this.registerEvent('edit');
        this.registerEvent('delete');
        this.registerEvent('move');
        this.registerEvent('load');
    }

    reset() {
        this.tasks = [];
    }

    createTask(title, description, priority) {
        const userID = UserService.getUserID();
        const taskID = this.taskTable.createTask(userID, title, description, priority);

        if (taskID >= 0) {
            this.tasks = this.taskTable.getTasks(userID);
            this.dispatchEvent('create', {id: taskID});
        }

        return taskID;
    }

    editTask(taskID, title, description, priority) {
        const userID = UserService.getUserID();
        const result = this.taskTable.editTask(userID, taskID, title, description, priority);

        if (result) {
            this.dispatchEvent('edit', {id: taskIDid});
        }

        return result;
    }

    deleteTask(taskID) {
        const userID = UserService.getUserID();
        const result = this.taskTable.deleteTask(userID, taskID);

        if (result) {
            this.tasks = this.taskTable.getTasks(userID);
            this.dispatchEvent('delete', {id: taskID});
        }

        return result;
    }

    moveTask(taskID, category) {
        const userID = UserService.getUserID();
        const result = this.taskTable.moveTask(userID, taskID, category);

        if (result) {
            this.dispatchEvent('move', {id: taskID, category});
        }

        return result;
    }

    canMoveTask(taskID, category) {
        return this.taskTable.canMoveTask(taskID, category);
    }

    getTask(taskID) {
        const userID = UserService.getUserID();
        return this.taskTable.getTask(userID, taskID);
    }

    loadTasks() {
        const userID = UserService.getUserID();
        this.tasks = this.taskTable.getTasks(userID);
        const result = (this.tasks && (this.tasks.length > 0));

        if (result) {
            this.dispatchEvent('load');
        }

        return result;
    }
}

export default new TaskService();
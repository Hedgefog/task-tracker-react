import Component from './component.jsx';

import React from 'react';
import {DropTarget} from 'react-dnd';

import Task from './task.component.jsx';

import TaskService from '../services/task.service.js';
import AppService from '../services/app.service.jsx';

const taskContainerTarget = {
  canDrop(props) {
    return true;
  },

  drop(props, monitor) {
    const category = parseInt(props.category);
    const taskID = monitor.getItem().taskID;

    TaskService.moveTask(taskID, category);
    AppService.setTaskDragged(taskID, false);
  }
};

function collect(connect, monitor) {
    return {
        connectDropTarget: connect.dropTarget(),
        isOver: monitor.isOver(),
        canDrop: monitor.canDrop()
    };
}

@DropTarget('task', taskContainerTarget, collect)
export default
class TaskContainer extends Component {
    static propTypes = {
        isOver: React.PropTypes.bool.isRequired,
        canDrop: React.PropTypes.bool.isRequired,
        connectDropTarget: React.PropTypes.func.isRequired,
        children: React.PropTypes.node
    }

    render() {
        const {connectDropTarget, isOver, canDrop, children, tasks} = this.props;
        return connectDropTarget(
            <div className="col-sm-6 col-md-3 task-container-wrapper">
                <h2 className="task-container-title">{this.props.title}</h2>
                <div className="task-container">
                    {
                        tasks.map((task, i)=> {
                            return (
                                <Task id={task.id} key={i} title={task.title} description={task.description} priority={task.priority}/>
                            );
                        })
                    }
                </div>
            </div>
        );
    }
}
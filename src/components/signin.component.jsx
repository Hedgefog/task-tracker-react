import Component from './component.jsx';

import React from 'react';

import UserService from '../services/user.service.js';

export default
class SignIn extends Component {
    constructor(props) {
        super(props);
        this.state = {
            username: "",
            password: ""
        };

        this.onUsernameChange = this.onUsernameChange.bind(this);
        this.onPasswordChange = this.onPasswordChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
    }

    render() {
        return (
            <form id="signInForm" onSubmit={this.onSubmit}>
                <h2>Sign In</h2>
                <input 
                    type="text" 
                    id="email" 
                    className="form-control" 
                    placeholder="Username" 
                    required="true"
                    autoFocus="true"
                    onChange={this.onUsernameChange}
                    value={this.state.username}
                />
                
                <input 
                    type="password" 
                    id="password" 
                    className="form-control" 
                    placeholder="Password" 
                    required="true" 
                    onChange={this.onPasswordChange}
                    value={this.state.password}
                />

                <button className="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
            </form>
        );
    }

    onUsernameChange(event) {
        this.setState({username: event.target.value});
    }

    onPasswordChange(event) {
        this.setState({password: event.target.value});
    }

    onSubmit(event) {
        event.preventDefault();
        UserService.signIn(this.state.username, this.state.password);
    }
}
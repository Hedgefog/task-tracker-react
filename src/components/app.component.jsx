import Component from './component.jsx';

import React from 'react';
import {DragDropContext} from 'react-dnd';
import HTML5Backend from 'react-dnd-html5-backend';

import TaskOverlay from './task-overlay.component.jsx';
import Navbar from './navbar.component.jsx';
import Content from './content.component.jsx';
import SignIn from './signin.component.jsx';

import Modal from './modal.component.jsx';

import UserService from '../services/user.service.js';
import TaskService from '../services/task.service.js';
import AppService from '../services/app.service.jsx';

@DragDropContext(HTML5Backend)
export default
class App extends Component {
    constructor(props) {
        super(props);
        
        this.state = {
            signedIn: false,
            overlayIsOpen: false,
            modalIsOpen: false,
            modalData: {
                title: "",
                body: null,
                okText: "",
                okCallback: null,
                closeCallback: null
            }

        };

        //Bind callbacks
        this.onSignIn = this.onSignIn.bind(this);
        this.onSignOut = this.onSignOut.bind(this);
        this.onTaskDnd = this.onTaskDnd.bind(this);
        this.onOpenModal = this.onOpenModal.bind(this);
        this.onCloseModal = this.onCloseModal.bind(this);
    }

    componentDidMount() {
        //Authorization
        UserService.addEventListener('signIn', this.onSignIn);
        UserService.addEventListener('signOut', this.onSignOut);

        //Task DnD
        AppService.addEventListener('task-drag', this.onTaskDnd);
        AppService.addEventListener('task-drop', this.onTaskDnd);

        //Modal
        AppService.addEventListener('open-modal', this.onOpenModal);
        AppService.addEventListener('close-modal', this.onCloseModal);

        //Try to sign in by saved token
        UserService.signInByToken();
    }
    
    componentWillUnmount() {
        //Authorization
        UserService.removeEventListener('signIn', this.onSignIn);
        UserService.removeEventListener('signOut', this.onSignOut);

        //Task DnD
        AppService.removeEventListener('task-drag', this.onTaskDnd);
        AppService.removeEventListener('task-drop', this.onTaskDnd);

        //Modal
        AppService.removeEventListener('open-modal', this.onOpenModal);
        AppService.removeEventListener('close-modal', this.onCloseModal);
    }

    render() {
        let content;

        if (this.state.signedIn) {
            content = (
                <div id="root">
                    {
                        function() {
                            if (this.state.overlayIsOpen) {
                                return <TaskOverlay/>;
                            }
                        }.bind(this)()
                    }
                    {
                        function() {
                            if (this.state.modalIsOpen) {
                                const {title, body, okText, okCallback, closeCallback} = this.state.modalData;
                                return <Modal 
                                    title={title} 
                                    okText={okText} 
                                    okCallback={okCallback} 
                                    closeCallback={closeCallback}
                                >
                                    {body}
                                </Modal>;
                            }
                        }.bind(this)()
                    }
                    <Navbar/>
                    <Content/>
                </div>
            );
        }
        else {
            content = (
                <div id="root">
                    <SignIn/>
                </div>
            );
        }

        return content;
    }

    openModal() {
        this.setState({modalIsOpen: true});
    }

    closeModal() {
        this.setState({modalIsOpen: false});
    }

    onSignIn() {
        this.setState({signedIn: true});

        TaskService.loadTasks();
        if (TaskService.tasks.length <= 0) {
            TaskService.createTask(
                "Create Task", 
                "Create your first task", 
                0
            );
        }
    }

    onSignOut() {
        this.setState({signedIn: false});
    }

    onTaskDnd() {
        setTimeout(()=> {
            this.setState({overlayIsOpen: AppService.taskDragged});
        }, 100);
    }

    onOpenModal(data) {
        this.setState({modalData: data});
        this.openModal();
    }

    onCloseModal() {
        this.closeModal();
    }
}
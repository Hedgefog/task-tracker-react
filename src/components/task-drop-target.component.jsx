import Component from './component.jsx';

import React from 'react';
import {DropTarget} from 'react-dnd';

import TaskService from '../services/task.service.js';
import AppService from '../services/app.service.jsx';

const dropTarget = {
    canDrop(props, monitor) {
        if (props.canDrop) {
            return props.canDrop(monitor.getItem());
        }
    },

    drop(props, monitor) {
        if (props.onDrop) {
            props.onDrop(monitor.getItem());
        }
        
        const taskID = monitor.getItem().taskID;
        AppService.setTaskDragged(taskID, false);
    }
};

function collect(connect, monitor) {
    return {
        connectDropTarget: connect.dropTarget(),
        isOver: monitor.isOver(),
        canDrop: monitor.canDrop()
    };
}

@DropTarget('task', dropTarget, collect)
export default
class TaskDropTarget extends Component {
    static propTypes = {
        isOver: React.PropTypes.bool.isRequired,
        canDrop: React.PropTypes.bool.isRequired,
        connectDropTarget: React.PropTypes.func.isRequired,
        children: React.PropTypes.node
    }

    render() {
        const {connectDropTarget, children} = this.props;
        return connectDropTarget(children);
    }
}
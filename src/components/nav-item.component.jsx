import Component from './component.jsx';

import React from 'react';

export default
class NavItem extends Component {
    render() {
        return (
            <li>
                <a href={this.props.href} onClick={this.props.onClick}>
                    {this.props.children}
                </a>
            </li>
        );
    }
}
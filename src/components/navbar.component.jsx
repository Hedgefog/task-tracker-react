import Component from './component.jsx';

import React from 'react';

import NavbarNav from './navbar-nav.component.jsx';
import NavItem from './nav-item.component.jsx';
import NavbarHeader from './navbar-header.component.jsx';
import NavUserMenu from './nav-user-menu.component.jsx';

import UserService from '../services/user.service.js';

export default
class Navbar extends Component {
    render() {
        return (
            <div className="navbar navbar-fixed-top navbar-inverse" role="navigation">
                <div className="container-fluid">
                    <NavbarHeader text="Task Tracker"/>
                    <div className="navbar-collapse collapse">
                        <NavbarNav right={false}>
                            <NavItem text="Home" href="#"/>
                        </NavbarNav>

                        <NavbarNav right={true}>
                            <NavUserMenu username={UserService.username}/>
                        </NavbarNav>
                    </div>
                </div>
            </div>
        );
    }
}
import Component from './component.jsx';

import React from 'react';

import TaskDropTarget from './task-drop-target.component.jsx';
import Task from './task.component.jsx';
import GlyphIcon from './glyphicon.component.jsx';

import TaskService from '../services/task.service.js';
import AppService from '../services/app.service.jsx';

export default
class TaskOverlayDelete extends Component {
    constructor(props) {
        super(props);

        this.onDrop = this.onDrop.bind(this);
        this.canDrop = this.canDrop.bind(this);
    }

    render() {
        return (
            <TaskDropTarget onDrop={this.onDrop} canDrop={this.canDrop}>
                <div className="task-controll" id="delete">
                    <GlyphIcon name="trash"/>
                    Delete
                </div>
            </TaskDropTarget>
        );
    }

    onDrop(item) {
        const taskID = item.taskID;
        AppService.openDeleteAcceptModal(taskID);
    }

    canDrop(item) {
        return true;
    }
}
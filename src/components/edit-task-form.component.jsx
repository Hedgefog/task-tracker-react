import Component from './component.jsx';

import React from 'react';

export default
class EditTaskForm extends Component {
    constructor(props) {
        super(props);

        this.state = {};

        this.onTitleChange = this.onTitleChange.bind(this);
        this.onDescriptionChange = this.onDescriptionChange.bind(this);
        this.onPriorityChange = this.onPriorityChange.bind(this);
    }

    render() {
        return (
            <form>
                <div className="form-group">
                    <label for="task-title" class="control-label">Title:</label>
                    <input 
                        type="text" 
                        id="taskTitle" 
                        className="form-control" 
                        placeholder="Title" 
                        required="true"
                        autoFocus="true"
                        onChange={this.onTitleChange}
                        value={this.state.title}
                    />
                </div>

                <div className="form-group">
                    <label for="task-description" class="control-label">Description:</label>
                    <textarea
                        type="text" 
                        id="taskDescription" 
                        className="form-control" 
                        placeholder="Description" 
                        required="true"
                        autoFocus="true"
                        onChange={this.onDescriptionChange}
                        value={this.state.description}
                    >
                    </textarea>
                </div>

                <div className="form-group">
                    <label for="task-priority" class="control-label">Priority:</label>
                    <input 
                        type="text" 
                        id="taskPriority" 
                        className="form-control" 
                        placeholder="Priority" 
                        required="true"
                        autoFocus="true"
                        onChange={this.onPriorityChange}
                        value={this.state.priority}
                    />
                </div>
            </form>
        );
    }

    onTitleChange(event) {
        this.setState({title: event.target.value}, ()=> {
            this.onChange();
        });
    }

    onDescriptionChange(event) {
        this.setState({description: event.target.value}, ()=> {
            this.onChange();
        });
    }

    onPriorityChange(event) {
        const value = event.target.value;
        let priority = 0;
        if (value) {
            priority = parseInt(value);

            if (!priority) {
                priority = 0;
            }
        }

        this.setState({priority}, ()=> {
            this.onChange();
        });
    }

    onChange() {
        const title = this.state.title;
        const description = this.state.description;
        const priority = this.state.priority;

        this.props.onChange({title, description, priority});
    }
}
import Component from './component.jsx';

import React from 'react';

import TaskOverlayDelete from './task-overlay.delete.component.jsx';
import GlyphIcon from './glyphicon.component.jsx';

export default
class TaskOverlay extends Component {
    render() {
        return (
            <div className="overlay" id="taskOverlay">
                <TaskOverlayDelete/>

                {/*<div className="task-controll" id="edit">
                    <GlyphIcon name="pencil"/>
                    Edit
                </div>*/}
            </div>
        );
    }
}
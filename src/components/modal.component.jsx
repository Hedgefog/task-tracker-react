    import Component from './component.jsx';

import React from 'react';

export default
class Modal extends Component {
    render() {
        return (
            <div className="modal fade in" id="appModal">
                <div className="modal-dialog">
                    <div className="modal-content">
                        <div className="modal-header">
                            <button type="button" className="close" data-dismiss="modal" aria-label="Close" onClick={this.props.closeCallback}>
                                <span aria-hidden="true">&times;</span>
                            </button>

                            <h4 className="modal-title" id="modalLabel">{this.props.title}</h4>
                        </div>

                        <div className="modal-body">
                            {this.props.children}
                        </div>
                        
                        <div className="modal-footer">
                            <button type="button" className="btn btn-primary" onClick={this.props.okCallback}>{this.props.okText}</button>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
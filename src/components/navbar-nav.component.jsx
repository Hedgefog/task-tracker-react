import Component from './component.jsx';

import React from 'react';

export default
class NavbarNav extends Component {
    render() {
        let className = 'nav navbar-nav';
        if (this.props.right) {
            className += ' navbar-right';
        }

        return (
            <ul className={className}>
                {this.props.children}
            </ul>
        );
    }
}
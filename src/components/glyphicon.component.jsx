import Component from './component.jsx';

import React from 'react';

export default
class GlyphIcon extends Component {
    render() {
        const className = "glyphicon glyphicon-" + this.props.name;
        return (
            <span className={className}></span>
        );
    }
}
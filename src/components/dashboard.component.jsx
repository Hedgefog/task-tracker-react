import Component from './component.jsx';

import React from 'react';

import TaskContainer from './task-container.component.jsx';

import TaskService from '../services/task.service.js';

const CategoryTitle = {
    0: "Pending",
    1: "Selected for Work",
    2: "Work in Progress",
    3: "Done"
};

export default
class Dashboard extends Component {
    constructor(props) {
        super(props);
        
        this.mounted = false;

        this.state = {
            tasks: TaskService.tasks
        };

        this.updateTasks = this.updateTasks.bind(this);
    }

    componentDidMount() {
        TaskService.addEventListener('create', this.updateTasks);
        TaskService.addEventListener('move', this.updateTasks);
        TaskService.addEventListener('delete', this.updateTasks);
        TaskService.addEventListener('load', this.updateTasks);
    }

    componentWillUnmount() {
        TaskService.removeEventListener('create', this.updateTasks);
        TaskService.removeEventListener('move', this.updateTasks);
        TaskService.removeEventListener('delete', this.updateTasks);
        TaskService.removeEventListener('load', this.updateTasks);
    }

    render() {
        return (
            <div className="col-sm-12 board">
                <div className="row task-containers">
                    {
                        Object.keys(CategoryTitle).map((key, i)=> {
                            const category = parseInt(key);
                            const tasks = this.getTasks(category);
                            const title = CategoryTitle[key];
                            
                            return (
                                <TaskContainer key={i} category={key} title={title} tasks={tasks}/>
                            );
                        })
                    }
                </div>
            </div>
        );
    }

    getTasks(category) {
        let tasks = [];
        
        this.state.tasks.map((task)=> {
            if (task.category === category) {
                tasks.push(task);
            }
        });

        return tasks;
    }

    updateTasks() {
        this.setState({tasks: TaskService.tasks});
    }
}
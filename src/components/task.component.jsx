import Component from './component.jsx';

import React from 'react';
import {DragSource} from 'react-dnd';

import AppService from '../services/app.service.jsx';

const taskSource = {
    beginDrag(props) {
        const taskID = props.id;
        AppService.setTaskDragged(taskID, true);
        return {taskID};
    }
};

function collect(connect, monitor) {
    return {
        connectDragSource: connect.dragSource(),
        isDragging: monitor.isDragging()
    };
}

@DragSource('task', taskSource, collect)
export default
class Task extends Component {
    static propTypes = {
        connectDragSource: React.PropTypes.func.isRequired,
        isDragging: React.PropTypes.bool.isRequired
    };

    render() {
        const {connectDragSource, isDragging} = this.props;

        let priorityClass = "priority ";
        if (this.props.priority < 4) {
            priorityClass += "low";
        } else if (this.props.priority < 8) {
            priorityClass += "normal";
        } else {
            priorityClass += "high";
        }

        let taskClass = "container task";
        if (AppService.taskDragged && this.props.id == AppService.draggedTaskID) {
            taskClass += " dragged";
        }

        return connectDragSource(
            <div className={taskClass}>
                <div className="title">{this.props.title}</div>
                <div className="description">{this.props.description}</div>
                <div className={priorityClass}></div>
            </div>
        );
    }
}
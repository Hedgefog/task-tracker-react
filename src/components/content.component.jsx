import Component from './component.jsx';

import React from 'react';

import Dashboard from './dashboard.component.jsx';

export default
class Content extends Component {
    render() {
        return (
            <div className="container-fluid" id="content">
                <div className="row">
                    <Dashboard/>
                </div>
            </div>
        );
    }
}
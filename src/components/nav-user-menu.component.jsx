import Component from './component.jsx';

import React from 'react';

import Dropdown from './dropdown.component.jsx';
import GlyphIcon from './glyphicon.component.jsx';
import NavItem from './nav-item.component.jsx';
import EditTaskForm from './edit-task-form.component.jsx';

import UserService from '../services/user.service.js';
import AppService from '../services/app.service.jsx';

export default
class NavUserMenu extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <Dropdown text={this.props.username}>
                    <NavItem href="#" onClick={()=>{
                        AppService.openCreateTaskModal();
                    }}>
                        <GlyphIcon name="plus"/>
                        Create Task
                    </NavItem>

                    <li role="separator" className="divider"></li>

                    <NavItem href="#" onClick={()=>{UserService.signOut();}}>
                        <GlyphIcon name="log-out"/>
                        Sign Out
                    </NavItem>
            </Dropdown>
        );
    }
}
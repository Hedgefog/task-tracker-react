import React from 'react';
import ReactDOM from 'react-dom';

import App from './src/components/app.component.jsx';

import UserService from './src/services/user.service.js';
import TaskService from './src/services/task.service.js';

const app = document.getElementById('app');
if (app) {
	ReactDOM.render(<App/>, app);
}

createUser();
function createUser() {
	UserService.signUp('admin', 'admin');
}
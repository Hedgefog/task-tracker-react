/* eslint-disable no-var, strict */
var webpack = require('webpack');
var WebpackDevServer = require('webpack-dev-server');
var config = require('./webpack.config');

new WebpackDevServer(webpack(config), {
  publicPath: config.output.publicPath,
  hot: true,
  historyApiFallback: true
}).listen(config.devServer.port, config.devServer.host, function (err) {
    if (err) {
      console.log(err);
    }
    console.log('Listening at localhost:' + config.devServer.port);
  });
